// Fetching necessary elements from index.html DOM

const payAmountElement = document.getElementById("payAmount");
const balanceAmountElement = document.getElementById("balanceAmount");
const loanBalanceElement = document.getElementById("loanBalance");
const loanElement = document.getElementById("loan");

const workButtonElement = document.getElementById("workButton");
const bankButtonElement = document.getElementById("bankButton");
const getLoanButtonElement = document.getElementById("getLoanButton");
const repayLoanButtonElement = document.getElementById("repayLoanButton");

const computersElement = document.getElementById("computers");
const priceElement = document.getElementById("price");
const computerImageElement = document.getElementById("computerImage");
const featureListElement = document.getElementById("featureList");

const computerNameElement = document.getElementById("computerName");
const descriptionElement = document.getElementById("description")
const buyButtonElement = document.getElementById("buyButton");




// Declaring JS variables with default values

// Starting off with 0 pay
let payBalance = 0;

// ...and 200 Kr in the bank
let bankBalance = 200.00;

// Starting off with no loan
let loanBalance = 0;

// Empty list to save computer object data in from Heroku API
let computers = [];

// Setting logical flag to track whether a loan is permitted
let loanIsPermitted = true;

// Placeholder variable for the computer object that is selected in the menu
let selectedComputer = null;




function requestLoanDialogue() {
    /*
        Handles any loan requests made by user and checks
        whether the loan is permitted. Also checks whether the input
        is acceptable. If successful, the loan is processed (see processLoanRequest).
    */

    if (loanIsPermitted) {
        const loanAmount = prompt("Enter the amount you wish to loan: ");

        if (loanAmount.match(/^[0-9]+$/) === null) {
            alert("Invalid input. Only whole numbers accepted.");

        } else if (loanAmount > 2 * bankBalance) {
            alert("Loan amount cannot be double your bank balance!");

        } else {
            processLoanRequest(loanAmount);
        }

    } else {
        alert("Buy a laptop, then you can loan again!")
    }
}




function processLoanRequest(loanAmount) {
    /*
        Process the loan request by logically adding and subtracting 
        money from the appropriate fields.

        * @param {int} the amount to be loaned as requested by the user.
    */

    loanIsPermitted = false; // No more loans are permitted until user buys a laptop

    // Subtracting the loan amount from the loan balance (seen as Debt on the website).
    loanBalance -= parseInt(loanAmount);
    loanBalanceElement.innerText = `${loanBalance} Kr`;

    // Adding the loan amount to the bank balance.
    bankBalance += parseInt(loanAmount);
    balanceAmountElement.innerText = `${bankBalance} Kr`;

    // Setting elements to visible so the user can 
    // see that they have a loan and can repay it with the repay button.
    loanElement.style.visibility = "visible";
    loanBalanceElement.style.visibility = "visible";
    repayLoanButtonElement.style.visibility = "visible";
}




function increasePayAmount() {
    /*
        Increases the pay balance by 100.
        Triggered when the user clicks the work button.
    */

    payBalance += 100;
    payAmountElement.innerText = `${payBalance} Kr`;
}




function transferPayToBank() {
    /*
        Transfers all money in the pay balance to the bank balance.
        However, if there is a loan, 10% of the money from the pay
        balance is used to pay off the loan before transferring
        the rest of the pay balance to the bank balance.
    */

    // Check if there is a loan, if yes, use 10% of the pay to pay it off
    if (loanBalance < 0) {
        repayLoan(0.1);
    }

    // Add the pay balance to bank balance
    bankBalance += payBalance;
    balanceAmountElement.innerText = `${bankBalance} Kr`;

    // Reset the pay balance, which has now been essentially transferred to the bank balance
    payBalance = 0;
    payAmountElement.innerText = `0 Kr`;
}




function repayLoanFully() {
    /*
        Used as a wrapper of repayLoan to pass parameters to eventListener.
        This is because eventListener does not accept functions with parameters.
        For details see function repayLoan.
    */

    if (payBalance === 0) {
        alert("Loan repayment requires pay funds!")
    } else {
        repayLoan(1);
    }
    
}




function repayLoan(proportionOfPayBalance) {
    /*
        Uses a proportion of the payBalance to pay off loanAmount.

        * @param {float} proportionOfPayBalance describes the proportion of payBalance to 
        be deducted from payBalance to pay off the loanAmount. By default,
        the entire payBalance is used. Values should be between 0 and 1.
    */

    const transferAmount = proportionOfPayBalance * payBalance;

    payBalance -= transferAmount;
    payAmountElement.innerText = `${payBalance} Kr`;

    loanBalance += transferAmount;

    // Too much debt ended up repaid or debt is settled
    if (loanBalance >= 0) {
        payBalance += loanBalance;
        loanBalance = 0;

        loanBalanceElement.innerText = "0 Kr";
        payAmountElement.innerText = `${payBalance} Kr`;

        loanElement.style.visibility = "hidden";
        loanBalanceElement.style.visibility = "hidden";
        repayLoanButtonElement.style.visibility = "hidden";

    // Otherwise update loanBalance
    } else {
        loanBalanceElement.innerText = `${loanBalance} Kr`;
        console.log(loanBalanceElement)
    }
    return;
}




/*  
    !!! Fetching data from API !!!

    Loading API data from heroku server for laptop information,
    names, and images.
    fetch returns a promise...used to access API data in JSON format.
*/
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
// Callback on execution of fetch used with .then
    .then(response => response.json())
    .then(data => computers = data)
    .then(computer => addComputersToMenu(computer));
// End of fetch

/*    
    Item 1 in JSON of API request: 
    {
    "id": 1,
    "title": "Classic Notebook",
    "description": "A little old, but turns on.",
    "specs": [
      "Has a screen",
      "Keyboard works, mostly",
      "32MB Ram (Not upgradable)",
      "6GB Hard Disk",
      "Comes with Floppy Disk Reader (Free) - Requires cable",
      "Good excersice to carry"
    ],
    "price": 200,
    "stock": 1,
    "active": true,
    "image": "assets/images/1.png"
    },
*/




const addComputersToMenu = (computers) => {
    /*
        Handles dynamic content display by loading items into the select menu
        and loading default values.

        Adds every JSON object to the select menu by applying
        the addComputerToMenu() function to every element
        in computers with .forEach.
        For details see addComputerToMenu.
        In addition, loads all the default values to be 
        displayed on-load.
    */
    computers.forEach(x => addComputerToMenu(x));

    // Load default on-load values
    selectedComputer = computers[0];
    priceElement.innerText = `${selectedComputer.price} NOK`;
    computerImageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedComputer.image;
    computerImageElement.alt = "Image of computer " + `$computers[0].title`;
    createFeatureList(selectedComputer);
    computerNameElement.innerText = selectedComputer.title;
    descriptionElement.innerText = selectedComputer.description;
}




function createFeatureList(computer) {
    /*
        Populate feature list below the computer selector drag-down
        menu.
    */

    // Ensures list is empty before adding items
    featureListElement.innerText = "";

    computer.specs.forEach(spec => {
        const listItem = document.createElement("li");
        listItem.innerText = spec;
        featureListElement.appendChild(listItem);
    })   
}




const addComputerToMenu = (computer) => {
    /*
        Adding every computer object title from API to the select menu.
    */

    const computerElement = document.createElement("option");
    //reference to JSON file with .id -> "id" part in JSON
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    computersElement.appendChild(computerElement);
}




const handleComputerMenuChange = e => {
    /*
        Handles any changes that are made by the user to the select menu.
        i.e. Items will dynamically change on the rest of the page depending
        on the selection.
    */

    // Assign the object that was select by the change event on the select
    // menu to selectedComputer
    selectedComputer = computers[e.target.selectedIndex];

    // innerHTML has security vulnerabilities -> innerText is used
    // Use the object properties of selectedComputer to populate the rest of the website
    priceElement.innerText = `${selectedComputer.price} NOK`;
    // Creating link to image
    computerImageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedComputer.image;
    // Forming alt message for image
    computerImageElement.alt = `Image of computer: ${selectedComputer.title}`;

    // Creating list of features for the computer
    createFeatureList(selectedComputer);

    // Adding title and description to bottom of the page
    computerNameElement.innerText = selectedComputer.title;
    descriptionElement.innerText = selectedComputer.description;
}




function buyComputer() {
    /*
        Handles user requests to buy a laptop.
        Checks for sufficient funds in bank and then
        responds if the buy is successful or not.
    */

    if (bankBalance < selectedComputer.price) {
        alert("Insufficient funds in your bank!")
    } else {
        bankBalance -= selectedComputer.price;
        loanIsPermitted = true;
        balanceAmountElement.innerText = `${bankBalance} Kr`
        alert(`Thank you for your purchase. You now own a ${selectedComputer.title}.`)
    }
}



// Adding event listener for the loan button to request a loan on-click
getLoanButtonElement.addEventListener("click", requestLoanDialogue);

// Adding event listener for the work button to increase pay on-click
workButtonElement.addEventListener("click", increasePayAmount);

// Adding event listener for the bank button to transfer pay on-click
bankButtonElement.addEventListener("click", transferPayToBank);

// Adding event listener for the repay button to repay any outstanding loan on-click
repayLoanButtonElement.addEventListener("click", repayLoanFully);

// Adding event listener for the buy button to process buy requests
buyButtonElement.addEventListener("click", buyComputer);

// Adding event listener for any change in the selection menu to change page content dynamically
computersElement.addEventListener("change", handleComputerMenuChange);